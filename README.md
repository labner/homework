# README #

A simple Java application that holds customer data and displays it in a front-end.  
Frontend is written with React.  
Backend was built on maven-archetype-webapp.

*CustomerController* implements methods:

***api/customers GET***    
retrieves all Customers

***api/customers POST***  
expects JSON data in request body
```json
    {
      "firstName": "Jaanika",
      "lastName": "Paanika",
      "dob": "2009-09-08",
      "email": "jaanika@tt.ee"
    }
```
Stores Customer data in *CustomerBase*
Or responds with error code 400 if there is already a customer with the same firstName and lastName

***api/customers?id={id} PUT***  
expects parameter {id} and JSON data in request body
Replaces Customer data in *CustomerBase*

***api/customers?id={id} DELETE***  
removes Customer data from CustomerBase    
Customer data is stored in CustomerBase class  
There is currently no error handling other than checking for duplicate names.

### Installing and running
Maven project includes tomcat plugin and maven can be used for building and running the application.

```
#!bash
git clone http://bitbucket.org/labner/homework.git
cd homework/
mvn clean tomcat7:run
```
Open [http://localhost:8080/api/customers](http://localhost:8080/api/customers)

Frontend was created with create-react-app. Files from build were copied to src/main/webapp

For UI open [http://localhost:8080](http://localhost:8080)

Fontend source is available at [https://bitbucket.org/labner/homework-front](https://bitbucket.org/labner/homework-front)

Functional requirements:

*  DONE Customer data can be added, changed and deleted (CRUD operations).
*  DONE A combination of user’s first and last name must remain unique; a user with the exact same full name cannot be added twice (check for duplication).
*  DONE User’s e-mail must be correctly formatted (validate format). Validation is done in front-end.
*  DONE Displayed data must be sortable by each column.
*  DONE Search box must implement auto-complete (ajax or similar) on user name. It is enough to demonstrate auto-complete working, actual search functionality is not needed. Uses react-autosuggest [https://github.com/moroshko/react-autosuggest]

Nonfunctional requirements:

*	DONE Write the back-end in Java 
*	DONE Write the front-end in JavaScript
*	DONE Write at least one unit-test for backend.
*	DONE Provide documentation on how to install and run the application.

Optional (“extra mile”):

*	Cover backend with unit tests.
*	DONE Write front-end using React.
