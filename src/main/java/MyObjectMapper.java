import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * Created by liina on 17.09.2019.
 */
public class MyObjectMapper {
    private static MyObjectMapper ourInstance = new MyObjectMapper();

    public static MyObjectMapper getInstance() { return ourInstance; }

    public ObjectMapper mapper = new ObjectMapper();

    private MyObjectMapper() {
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }
}
