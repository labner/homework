import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.io.IOUtils;

@WebServlet("/api/customers")
public class CustomerController extends HttpServlet {

    private CustomerBase customerBase = new CustomerBase();

    // doGet meetod, naitab customerListi sisu
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Customer> customerList = customerBase.getCustomers();
        response.setContentType("application/json");
        MyObjectMapper.getInstance().mapper.writeValue(response.getOutputStream(), customerList);
    }

    // doPost meetod, mis sisestusi salvestab
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String input = IOUtils.toString(request.getInputStream(),"UTF-8");
        Customer customer = MyObjectMapper.getInstance().mapper.readValue(input, Customer.class);
        if (customerBase.isNewName(customer)){
            customerBase.addCustomer(customer);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println("Duplicate name");
        }
    }

    //doPut
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String input = IOUtils.toString(request.getInputStream(),"UTF-8");
        Customer customer = MyObjectMapper.getInstance().mapper.readValue(input, Customer.class);
        customerBase.updateCustomer(customer);
    }

    // doDelete meetod, mis Customer-id ara kustutab
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         if (request.getParameterMap().containsKey("id")) {
            customerBase.deleteCustomerById(Integer.parseInt(request.getParameter("id")));
        }
    }
}