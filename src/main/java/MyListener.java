import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class MyListener implements ServletContextListener{
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        CustomerBase.initialize();
        CustomerBase.loadSampleData();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}