import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class CustomerBase {
    private static List<Customer> customerList;
    private static int counter;

    public static void initialize() {
        customerList = new ArrayList<>();
        counter = 1;
    }

    public static void loadSampleData() {
        addCustomer(new Customer("Liina","Abner", LocalDate.of(1973,2,17),"liina.abner@gmail.com"));
        addCustomer(new Customer("Iti-Kärt","Õunapuu", LocalDate.of(1993,12,3),"iti@gmail.com"));
        addCustomer(new Customer("Eevald","Seevald", LocalDate.of(1923,04,30),"eevald@seevald.com"));
    }

    public static void addCustomer(Customer customer) {
        customer.setId(counter);
        customerList.add(customer);
        counter++;
    }
    public List<Customer> getCustomers() {
        return customerList;
    }

    public void deleteCustomerById(int id) {
        customerList.removeIf(customer -> customer.getId() == id);
    }

    public void updateCustomer(Customer customer) {
        customerList = customerList.stream()
                .map(c -> c.getId() == customer.getId() ? customer : c)
                .collect(Collectors.toList());
    }

    public boolean isNewName(Customer customer) {
        Customer existingCustomer = customerList.stream()
                .filter(c -> (c.getFirstName().equals(customer.getFirstName())
                        &&  (c.getLastName().equals(customer.getLastName()))))
                .findAny()
                .orElse(null);
        if (existingCustomer == null) {
            return true;
        }
        return false;
    }
}