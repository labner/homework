import org.junit.jupiter.api.Test;
import org.mockito.internal.util.reflection.FieldSetter;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CustomerControllerTest {

    @Test
    void checkCustomerIsAdded() throws IOException {
        String input = "{    \"firstName\": \"Jaanika\",\n" +
                "        \"lastName\": \"Paanika\",\n" +
                "        \"dob\": \"2009-09-08\",\n" +
                "        \"email\": \"tiina@tt.ee\"\n" +
                "    }";
        Customer customer =  MyObjectMapper.getInstance().mapper.readValue(input, Customer.class);
        CustomerBase cbase = new CustomerBase();
        cbase.initialize();
        cbase.addCustomer(customer);
        assertEquals(cbase.getCustomers().size(),1);
    }

    @Test
    public void jsonIsWrittenToOutputStream() throws IOException, ServletException, NoSuchFieldException {
        HttpServletRequest mockrequest = mock(HttpServletRequest.class);
        HttpServletResponse mockresponse = mock(HttpServletResponse.class);
        CustomerBase mockBase = mock(CustomerBase.class);
        List<Customer> customerList = new ArrayList<>();
        Customer customer = new Customer("Iti","Biti",LocalDate.of(1973,2,17),"moo@moo.ee");
        customer.setId(2);
        customerList.add(customer);
        CustomerController controller = new CustomerController();
        FieldSetter.setField(controller, controller.getClass().getDeclaredField("customerBase"), mockBase);
        when(mockBase.getCustomers()).thenReturn(customerList);
        final StubServletOutputStream stubOutputStream = new StubServletOutputStream();
        when(mockresponse.getOutputStream()).thenReturn(stubOutputStream);
        controller.doGet(mockrequest, mockresponse);
        assertEquals(stubOutputStream.getContent(),"[{\"id\":2,\"firstName\":\"Iti\",\"lastName\":\"Biti\",\"dob\":\"1973-02-17\",\"email\":\"moo@moo.ee\"}]");

    }
}

class StubServletOutputStream extends ServletOutputStream {
    public ByteArrayOutputStream baos = new ByteArrayOutputStream();
    public void write(int i) throws IOException {
        baos.write(i);
    }
    public String getContent() {
        return baos.toString();
    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setWriteListener(WriteListener writeListener) {
    }
}